# SCCN Model - Stellartrip

## Project

The goal of the SCCN project is to evaluate the possibilities of machine learning and deep learning technologies, in particular in the practice of amateur spectroscopy in astronomy.

More info here : [https://sccn.stellartrip.net](https://sccn.stellartrip.net)