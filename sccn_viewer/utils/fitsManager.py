##########
# IMPORT #
##########

from PySide2 import QtWidgets, QtCore
from astropy.io import fits
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import (NavigationToolbar2QT as NavigationToolbar)
from matplotlib.widgets import Cursor, RectangleSelector, TextBox
from numpy import arange, amin, amax, average
from detector import Detector
import matplotlib.pyplot as plt
import tempfile
import re
import os
import shutil
import sys

class FitsManager:
    """Class that manages FITS/FIT files in order to be able to easily manipulate the data and display the photo.

    Methods
    -------
    loadFitsFile(filePath, ui)
        Retrieves the data present in the header and displays the photograph in the software
    """


    @staticmethod
    def loadFitsFile(filePath, ui):
        """Retrieves the data present in the header and displays the photograph in the software

        Parameters
        ----------
        filePath : str
            contains the path to the file to be loaded
        ui : MyWindow
            the software interface where the data and the photograph will be displayed
        """

        print("Loading")

        FitsManager.openedFile = filePath;

        embedMPL = ui.findChildren(QtWidgets.QVBoxLayout, 'embedMPL')[0]
        regression = ui.findChildren(QtWidgets.QVBoxLayout, 'regression')[0]

        for i in reversed(range(embedMPL.count())):
            embedMPL.itemAt(i).widget().setParent(None)

        for i in reversed(range(regression.count())):
            regression.itemAt(i).widget().setParent(None)

        hdul = fits.open(filePath)
        fig, fwhm, reg = Detector.detectFromFit(hdul[0].data)

        canvas = FigureCanvas(fig)
        embedMPL.addWidget(canvas)
        embedMPL.addWidget(NavigationToolbar(canvas, canvas))

        image_data = fits.getdata(filePath)
        
        fig = plt.figure()
        ax1 = fig.add_subplot(2, 2, 1)
        ax2 = fig.add_subplot(2, 2, 2)
        ax3 = fig.add_subplot(2, 2, 3)
        ax4 = fig.add_subplot(2, 2, 4)
        
        ax1.set_title("Degree 1")
        ax2.set_title("Degree 2")
        ax3.set_title("Degree 3")
        ax4.set_title("Degree 4")

        ax1.plot(range(0, image_data.shape[1]), reg[0](range(0, image_data.shape[1])), label="degree 1")
        ax2.plot(range(0, image_data.shape[1]), reg[1](range(0, image_data.shape[1])), label="degree 2")
        ax3.plot(range(0, image_data.shape[1]), reg[2](range(0, image_data.shape[1])), label="degree 3")
        ax4.plot(range(0, image_data.shape[1]), reg[3](range(0, image_data.shape[1])), label="degree 4")
        
        fig.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=0.5)

        canvas = FigureCanvas(fig)
        regression.addWidget(canvas)
        regression.addWidget(NavigationToolbar(canvas, canvas))



        formule1 = QtWidgets.QLineEdit("Degree 1 : %s" % format(float("{:e}".format(reg[0][1])), '.3g') + "x + " + format(float("{:e}".format(reg[0][0])), '.3g'))
        formule2 = QtWidgets.QLineEdit("Degree 2 : %s" % format(float("{:e}".format(reg[1][2])), '.3g') + "x^2 + " + format(float("{:e}".format(reg[1][1])), '.3g') + "x + " + format(float("{:e}".format(reg[1][0])), '.3g'))
        formule3 = QtWidgets.QLineEdit("Degree 3 : %s" % format(float("{:e}".format(reg[2][3])), '.3g') + "x^3 + " + format(float("{:e}".format(reg[2][2])), '.3g') + "x^2 + " + format(float("{:e}".format(reg[2][1])), '.3g') + "x + " + format(float("{:e}".format(reg[2][0])), '.3g'))
        formule4 = QtWidgets.QLineEdit("Degree 4 : %s" % format(float("{:e}".format(reg[3][4])), '.3g') + "x^4 + " + format(float("{:e}".format(reg[3][3])), '.3g') + "x^3 + " + format(float("{:e}".format(reg[3][2])), '.3g') + "x^2 + " + format(float("{:e}".format(reg[3][1])), '.3g') + "x + " + format(float("{:e}".format(reg[3][0])), '.3g'))

        formule1.setReadOnly(True)
        formule2.setReadOnly(True)
        formule3.setReadOnly(True)
        formule4.setReadOnly(True)

        regression.addWidget(formule1)
        regression.addWidget(formule2)
        regression.addWidget(formule3)
        regression.addWidget(formule4)

        menuOpened_file = ui.findChildren(QtWidgets.QMenu, 'menuOpened_file')[0]
        menuOpened_file.setTitle("Opened File : "+re.search(r"(/|\\)(\b[^\\/]*\b)(\.fits|\.fit)",FitsManager.openedFile).group(2))

        print("Done")

        

        

