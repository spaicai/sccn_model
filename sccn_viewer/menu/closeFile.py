##########
# IMPORT #
##########
from PySide2 import QtWidgets
from PySide2.QtWidgets import*

class closePicture(QWidget):
    """Controller of the Close picture button. Reset the software
    
    '''

    Attibutes
    ----------
    parent : MyWindow
        Window containing the image and data to be reset

    '''

    Methods
    -------
    close()
        Closes the image and resets the displayed data and statistics
    """

    def __init__(self, parent):
        """
        Parameters
        ----------
        parent : MyWindow
            Window containing the image and data to be reset
        """
        self.parent = parent

    def close(self):

        embedMPL = self.parent.ui.findChildren(QtWidgets.QVBoxLayout, 'embedMPL')[0]
        for i in reversed(range(embedMPL.count())):
            embedMPL.itemAt(i).widget().setParent(None)

        regression = self.parent.ui.findChildren(QtWidgets.QVBoxLayout, 'regression')[0]
        for i in reversed(range(regression.count())):
            regression.itemAt(i).widget().setParent(None)

            

        menuOpened_file = self.parent.ui.findChildren(QtWidgets.QMenu, 'menuOpened_file')[0]
        menuOpened_file.setTitle("")

    
