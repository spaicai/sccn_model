#########
# IMPORT #
#########

from PySide2 import QtGui, QtWidgets, QtCore
from PySide2.QtUiTools import QUiLoader
from astropy.io import fits
import sys, re

from menu.quit import quit
from menu.openFile import fileChooser
from menu.closeFile import closePicture
from utils.fitsManager import FitsManager
from detector import Detector
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import (NavigationToolbar2QT as NavigationToolbar)

# ------------------ MyWindow ------------------
class MyWindow(QtWidgets.QMainWindow):
    """Main display class
    
    Attributes
    ----------
    ui : PySide2.QtWidgets.QMainWindow
        main display
    
    Methods
    -------
    load()
        loads the interface. Must be followed by .show() method to display the window
    dragEnterEvent(event)
        Allows to retrieve the information of the file that is put on the window
    dropEvent(event)
        Allows you to manage the actions that will be performed when the file is dropped on the window 
    """

    def load(self):
        """loads the interface. Must be followed by .show() method to display the window"""
        self.setWindowTitle("Stellartrip SCCN Viewer")
        loader = QUiLoader()
        self.ui = loader.load('interface.ui')
        print()
        print("-"*24)
        print("Starting Stellartrip SCCN Viewer app", "\n")
        self.setAcceptDrops(True)
        self.ui.setAcceptDrops(True)

        fc = fileChooser(parent=self)
        cp = closePicture(parent=self)
        self.ui.actionOpen_file.triggered.connect(lambda checked: fc.initUI())
        self.ui.actionClose_Picture.triggered.connect(lambda checked: cp.close())
        self.ui.actionQuit.triggered.connect(quit)

        self.setCentralWidget(self.ui)

    def dragEnterEvent(self, event):
        """Allows to retrieve the information of the file that is put on the window

        Parameters
        ----------
        event : PySide2.QtGui.QDragEnterEvent
            Event that is triggered when the file is on the window. Contains information such as the path to the file.
        """

        mime = event.mimeData()
        urls = mime.urls()[0]
        if len(mime.urls()) == 1 and re.match("PySide2.QtCore.QUrl\('[\w\W]*.(fits|fit)\'\)", str(urls)) != None :
            event.acceptProposedAction()

    def dropEvent(self, event):
        """Allows you to manage the actions that will be performed when the file is dropped on the window 
    
        Parameters
        ----------
        event : PySide2.QtGui.QDragEnterEvent
            Event that is triggered when the file is on the window. Contains information such as the path to the file.
        """

        for url in event.mimeData().urls():
            file_name = url.toLocalFile()
            FitsManager.loadFitsFile(file_name, self)




app = QtWidgets.QApplication(sys.argv)
app.setWindowIcon(QtGui.QIcon('stellartrip.png'))
window = MyWindow()
window.load()
window.resize(1280, 720)
window.show()
app.exec_()
